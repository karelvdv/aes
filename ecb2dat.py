#!/usr/bin/env python
# converts the test vectors in ecb_tbl.txt to binary files that are much easier to read.

f = open("ecb_tbl.txt", "rb")

ecbs = {}
ecbn = ""
ecb = []

for line in f:
	line = line[0:-2]
	if line:
		k,v = line.split("=")
		if k == "KEYSIZE":
			if ecb:
				ecbs[ecbn] = ecb
			ecbn = v
			ecb = []
		elif k == "PT":
			pt = v
		elif k == "KEY":
			key = v
		elif k == "CT":
			ct = v
			ecb.append([pt, key, ct])

ecbs[ecbn] = ecb

for n in ecbs.iterkeys():
    import binascii
    f = open("ecb%s.dat" % n, "wb")
    for pt, k, ct in ecbs[n]:
        f.write(binascii.unhexlify(k))
        f.write(binascii.unhexlify(pt))
        f.write(binascii.unhexlify(ct))
